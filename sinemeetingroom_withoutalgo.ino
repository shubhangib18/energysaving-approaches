
/**************************************** Carrier AC with ESP8266 Wemos D1 mini interface with ds18b20 temprature sensor ************************************************************/
/*  TSOP     1738                  = D5 (14)
    IRLED   black                  = D2 (4)
    DS18b20 Temprature sensor      = D6 (12)
*/

#include <WiFi.h>
#include "esp_system.h"
const int wdtTimeout = 120000;  //time in ms to trigger the watchdog
hw_timer_t *timer = NULL;
#include <Arduino.h>
#include <IRrecv.h>
#include <IRremoteESP8266.h>
#include <IRac.h>
#include <IRtext.h>
#include <IRutils.h>
#include <IRsend.h>
#include <PubSubClient.h>
//#include<ir_Universal.h>
#include <ArduinoJson.h>
#include <TaskScheduler.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <HTTPClient.h>
#include <NTPClient.h>
#include "SPIFFS.h"
#include <esp32fota.h>
#include <ESP32Ping.h>
#include <Wire.h>
#include "SHTSensor.h"

//#include<ir_Universal.h>
//const char* ssid;     //Irvine-Rapid_C05
//const char* password; 
String WIFI_SSID;
String WIFI_PASSWORD;
String DEVICE_NAME = "Living_Things";
unsigned long timeout = 0;
const uint16_t kRecvPin = 18;                                  // D5 of esp8266
const uint32_t kBaudRate = 115200;
const uint16_t kCaptureBufferSize = 1024;
const uint16_t kIrLed = 17;
const char* mqtt_server = "3.134.223.117";
const int mqtt_port = 4132;
const char *mqtt_user = "";
const char *mqtt_pass = "";
const long utcOffsetInSeconds = 19800;
int version_no = 14;
byte flag_pir = 0;
byte flag_reset = 1;
byte count_stop = 0;
String da;
int i = 0;
uint8_t m;
uint32_t count_pir = 0;
const char* remote_host = "www.google.com";
int rst_count = 0;
int key;
int counter;
String st_temp[4] = "";
String st_data[4] = "";
String setpoint, rawData_, State_;
float temp;
String MAC_ID;
//String temper;
String temper1;
String temperature = "24";
String Power = "On";
String Fan_speed = "3";
String AC_Mode = "0";
byte flag_inturrupt = 0;
bool ota_update = 0;
bool remote_config = 0;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "asia.pool.ntp.org", utcOffsetInSeconds);
WiFiServer server(80);
WiFiClient espClient;
esp32FOTA esp32FOTA("esp32-fota-http", version_no);

#define timeSeconds 10

// Set GPIOs for LED and PIR Motion Sensor
const int led = 26;
//const int le = 17;
const int motionSensor = 19;

// Timer: Auxiliary variables
unsigned long now = millis();
unsigned long lastTrigger = 0;
boolean startTimer = false;


#if DECODE_AC
const uint8_t kTimeout = 50;
#else
const uint8_t kTimeout = 15;
#endif
const uint16_t kMinUnknownSize = 12;
#define LEGACY_TIMING_INFO false
#define DS18B20 4                                                                  //DS18B20 is connected to D6


SHTSensor sht(SHTSensor::SHT3X);
float humidity;


OneWire ourWire(DS18B20);
DallasTemperature sensor(&ourWire);

IRsend irsend(kIrLed);
IRHitachiAc1 ac(kIrLed);
IRrecv irrecv(kRecvPin, kCaptureBufferSize, kTimeout, true);

decode_results results;
void send_DP();
float read_temp();
void callback(char* topic, byte * payload, unsigned int length);
PubSubClient client(mqtt_server, 4132, callback, espClient);
void deleteFile(fs::FS &fs, const char * path);
Task t1(900000, TASK_FOREVER, &send_DP);
Scheduler runner;
byte buf = 0;



void IRAM_ATTR resetModule() {
  ets_printf("reboot\n");
  esp_restart();
}


/**********************************************************************************************************************************/

void IRAM_ATTR detectsMovement() {
  Serial.println("MOTION DETECTED!!!");
  count_pir = 0;
  startTimer = true;
  lastTrigger = millis();
  flag_reset = 1;
  if (flag_pir == 1)
  {
    flag_inturrupt = 1;
  }

}
/**************************************************************************************************************/

void write_spiffs(String WIFI_SSID, String WIFI_PASSWORD, String DNAME, int ET)
{

  Serial.print("Writing credentials......");
  Serial.print("SSID:");
  Serial.print(WIFI_SSID);
  Serial.print("Password:");
  Serial.print(WIFI_PASSWORD);
  Serial.print("DNAME:");
  Serial.print(DNAME);
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  File file = SPIFFS.open("/config.json", FILE_WRITE);
  if (!file) {
    Serial.println("There was an error opening the file for writing");
    return;
  }
  String jsonstr = "{\"ServerName\":\"" + WIFI_SSID + "\",\"Password\":\"" + WIFI_PASSWORD +  "\",\"DNAME\":\"" + DNAME + "\"}";
  // Serial.print(jsonstr);
  if (file.print(jsonstr)) {
    //Serial.println("File was written");
  } else {
    //Serial.println("File write failed");
  }

  file.close();

  File file2 = SPIFFS.open("/config.json");
  //Serial.print("FILE: ");
  //Serial.println(file2.name());
  if (!file2) {
    //Serial.println("Failed to open file for reading");
    return;
  }

  //Serial.println("File Content:");

  while (file2.available()) {

    Serial.write(file2.read());
  }

  file2.close();
  delay(100);
  if (ET == 0)
  {
    ESP.restart();
  }
}
/***********************************************************************************************************************/
// load configuration at device initialization
bool load_config() {
  File configFile = SPIFFS.open("/config.json", "r");
  Serial.print("FILE: ");
  Serial.println(configFile.name());
  if (!configFile) {
    Serial.println("Failed to open config file");
    return false;
  }

  size_t size = configFile.size();
  if (size > 1024) {
    Serial.println("Config file size is too large");
    return false;
  }
  DynamicJsonDocument jsonBuffer(256);
  if ( size == 0 ) {
    return false;
  }
  else
  {
    std::unique_ptr<char[]> buf (new char[size]);
    configFile.readBytes(buf.get(), size);
    deserializeJson(jsonBuffer, buf.get());
    JsonObject root = jsonBuffer.as<JsonObject>();
    //Serial.print(root);
    for (JsonPair kv : root)
    {
      String da = kv.key().c_str();
      if (da == "ServerName")
      {
        String W_SSID = root["ServerName"];
        WIFI_SSID = W_SSID.c_str();
      }
      if (da == "Password")
      {
        String W_PASSWORD = root["Password"];
        WIFI_PASSWORD = W_PASSWORD.c_str();
      }
      if (da == "DNAME")
      {
        String D_NAME = root["DNAME"];
        DEVICE_NAME = D_NAME.c_str();
      }
    }
  }
  Serial.print("Loaded serverName: ");
  Serial.println(WIFI_SSID);
  Serial.print("Loaded accessToken: ");
  Serial.println(WIFI_PASSWORD);
  return true;
}
/************************************************************************************************************/
void deleteFile(fs::FS &fs, const char * path) {
  Serial.printf("Deleting file: %s\r\n", path);
  if (fs.remove(path)) {
    Serial.println("- file deleted");
    delay(200);
    ESP.restart();
  } else {
    Serial.println("- delete failed");
  }
}

/************************************************************************************************************/
bool setup_wifi(int timeout)
{
  char buf1[30];
  char buf2[30];
  char buf3[30];
  uint64_t start = millis();
  delay(10);
  server.close();
  WiFi.softAPdisconnect (true);
  if (!load_config())
  {
    WiFi.disconnect();
    Serial.println(DEVICE_NAME);
    int len = DEVICE_NAME.length();
    DEVICE_NAME.toCharArray(buf3, len);
    Serial.println("Configuring access point...");
    // You can remove the password parameter if you want the AP to be open.
    WiFi.softAP(buf3, "12345678");
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    server.begin();
    Serial.println("Server started");
    m = 1;
  }
  else
  {
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(WIFI_SSID);
    int len = WIFI_SSID.length();
    WIFI_SSID.toCharArray(buf1, len);
    len = WIFI_PASSWORD.length();
    WIFI_PASSWORD.toCharArray(buf2, len);
    Serial.print("updated SSID:");
    Serial.print(buf1);
    Serial.print("updated Password:");
    Serial.print(buf2);
    //WiFi.begin("NETGEAR2","$ine1234$");
    WiFi.begin(buf1, buf2);
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(100);
      Serial.print(".");
      m = 0;
      if (millis() - start > timeout)
      {
        WiFi.disconnect();
        Serial.println("Configuring access point...");

        // You can remove the password parameter if you want the AP to be open.
        Serial.println(DEVICE_NAME);
        int len = DEVICE_NAME.length();
        DEVICE_NAME.toCharArray(buf3, len);
        WiFi.softAP(buf3, "12345678");
        IPAddress myIP = WiFi.softAPIP();
        Serial.print("AP IP address: ");
        Serial.println(myIP);
        server.begin();
        Serial.println("Server started");
        m = 1;
        return false;
      }
    }
  }
  Serial.println();
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  if (Ping.ping(remote_host)) {
    Serial.println("Success!!");
    m = 0;
  } else {
    Serial.println("Error :(");
    //m = 1;

  }
  return true;

}
/********************************************************************************************************/
// change wifi configuration SSID, Password and Device name

void wifi_config()
{
  String data, WI_SSID, WI_PASSWORD, stat, DNAME;

  while (1)
  {
    WiFiClient client = server.available();
    if (client) {
      Serial.println("New Client.");
      String currentLine = "";
      if (client.connected()) {
        if (client.available()) {

        }
        Serial.print("\n");
        data = client.readStringUntil('\n');
        Serial.print("data");
        Serial.print(data);
        int len = data.length();
        data.remove(len - 9);
        len = data.length();
        data.remove(0, 5);
        if (data.startsWith("SSID"))
        {
          Serial.print("json format");
          data.remove(0, 5);
          len = data.length();
          Serial.print("SSID:");
          Serial.print(data);
          WI_SSID = data;
          Serial.print("\n");
        }
        if (data.startsWith("PASSWORD"))
        {
          Serial.print("json format");
          data.remove(0, 9);
          len = data.length();
          Serial.print("PASSWORD:");
          Serial.print(data);
          WI_PASSWORD = data;
          Serial.print("\n");
        }
        if (data.startsWith("DNAME:"))
        {
          Serial.print("json format");
          data.remove(0, 6);
          len = data.length();
          Serial.print("DEVICE NAME:");
          Serial.print(data);
          DNAME = data;
          Serial.print("\n");
        }
        if (data.startsWith("ok"))
        {
          Serial.print("json format");
          Serial.print("DONE:");
          Serial.print(data);
          stat = "ok";
          Serial.print("\n");
        }
        client.println("HTTP/1.1 200 OK");
        client.println("Access-Control-Allow-Origin: *");
        client.println("Origin,X-Requested-With,Content-type,Accept");
        client.println();
        client.print("Got response");
        client.println();
        client.stop();
        Serial.println("Client Disconnected.");
        if (stat == "ok")
        {
          write_spiffs(WI_SSID, WI_PASSWORD, DNAME, 0);
        }
      }
    }
  }
}

/*******************************************************************************************************************/

/************************************************************************************************************************/
void send_pir_command()
{
  ac.begin();
  ac.on();
  ac.setTemp(temperature.toInt());
  //String s = "kMitsubishiAc";
  if (AC_Mode == "2")
    ac.setMode(kHitachiAc1Auto);
  else if (AC_Mode == "1")
    ac.setMode(kHitachiAc1Dry);
  else if (AC_Mode == "0") {
    /*  ac.on();
       ac.setTemp(temperature.toInt());*/
    ac.setMode(kHitachiAc1Cool);                                                                          // Send a raw data capture at 38kHz.
    delay(2000);
  }
  else if (AC_Mode == "4") {
    // ac.setMode(kMitsubishiAcHeat);

    ac.setMode(kHitachiAc1Heat);                                                                            // Send a raw data capture at 38kHz.
    delay(2000);
  }
  /* else if (AC_Mode == "4")
     ac.setMode(kMitsubishiAcFan);
  */
  // String s1 = "kMitsubishiAcFan";
  if (Fan_speed == "1")
    ac.setFan(5);
  else if (Fan_speed == "2")
    ac.setFan(3);
  else if (Fan_speed == "3")
    ac.setFan(0);
  else if (Fan_speed == "4")
    ac.setFan(1);
  ac.send();

}
void callback(char* topic, byte * payload, unsigned int length)
{
  Serial.print("Message arrived [");
  buf = 1;
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  StaticJsonDocument<600> doc;
  deserializeJson(doc, payload, length);
  JsonObject root = doc.as<JsonObject>();
  for (JsonPair kv : root)
  {
    Serial.println(kv.key().c_str());

    da = kv.key().c_str();
    Serial.print(da);
    String temper = root[da];
    temper1 = temper;
    Serial.print(temper);
    if (da == "Temp")
    {
      temperature = temper;
    }
    if (da == "Power")
    {
      Power = temper;
    }

    if (da == "d")
    {
      int tem = root[da]["0"];
      if (tem == 1)
      {
        Power = "on";
      }
      else {
        Power = "off";
      }
    }
    if (da == "Mode")
    {
      Power = "on";
      AC_Mode = temper;
    }
    if (da == "Fan")
    {
      Power = "on";
      Fan_speed = temper;
    }
    if (da == "e")
    {
      if (temper == "OTA") {
        ota_update = 1;
      }
    }
    if (da == "e")
    {
      if (temper == "FR") {
        Serial.print("Factory reseting...");
        deleteFile(SPIFFS, "/config.json");
      }
    }
  }
  StaticJsonDocument<450>doc1;
  Serial.print("temperature -->");
  Serial.print(temperature);
  Serial.print("Power -->");
  Serial.print(Power);
  Serial.print("Mode -->");
  Serial.print(AC_Mode);
  Serial.print("Fan -->");
  Serial.print(Fan_speed);
  if (temper1 != "ACK")
  {
    if ((Power == "on")) {
       ac.begin();
       ac.setModel(R_LT0541_HTA_A);
       ac.setPower(true);
      // ac.setPower(false);
      if (AC_Mode == "0") {
         ac.setMode(kHitachiAc1Cool);
      if (Fan_speed == "1")
        ac.setFan(kHitachiAc1FanHigh);
      else if (Fan_speed == "2")
        ac.setFan(kHitachiAc1FanMed);
      else if (Fan_speed == "3")
        ac.setFan(kHitachiAc1FanAuto);
      else if (Fan_speed == "4")
        ac.setFan(kHitachiAc1FanLow); 
      ac.setTemp(temperature.toInt());
      }
      
      if (AC_Mode == "1")
      {
       ac.setMode(kHitachiAc1Dry);
      if (Fan_speed == "1")
        ac.setFan(kHitachiAc1FanHigh);
      else if (Fan_speed == "2")
        ac.setFan(kHitachiAc1FanMed);
      else if (Fan_speed == "3")
        ac.setFan(kHitachiAc1FanAuto);
      else if (Fan_speed == "4")
        ac.setFan(kHitachiAc1FanLow);
      ac.setTemp(temperature.toInt());
      }
      
      if (AC_Mode == "2")
      {
        ac.setMode(kHitachiAc1Auto);
      if (Fan_speed == "1")
        ac.setFan(kHitachiAc1FanHigh);
      else if (Fan_speed == "2")
        ac.setFan(kHitachiAc1FanMed);
      else if (Fan_speed == "3")
        ac.setFan(kHitachiAc1FanAuto);
      else if (Fan_speed == "4")
        ac.setFan(kHitachiAc1FanLow);
      ac.setTemp(temperature.toInt());
      }
      
      if (AC_Mode == "3")
      {
      ac.setMode(kHitachiAc1Heat);                                                                            // Send a raw data capture at 38kHz.
      if (Fan_speed == "1")
        ac.setFan(kHitachiAc1FanHigh);
      else if (Fan_speed == "2")
        ac.setFan(kHitachiAc1FanMed);
      else if (Fan_speed == "3")
        ac.setFan(kHitachiAc1FanAuto);
      else if (Fan_speed == "4")
        ac.setFan(kHitachiAc1FanLow);
      ac.setTemp(temperature.toInt());
      }
      
      if (AC_Mode == "4")
      {
       ac.setMode(kHitachiAc1Fan);                                                                            // Send a raw data capture at 38kHz.                                    
      if (Fan_speed == "1")
        ac.setFan(kHitachiAc1FanHigh);
      else if (Fan_speed == "2")
        ac.setFan(kHitachiAc1FanMed);
      else if (Fan_speed == "3")
        ac.setFan(kHitachiAc1FanAuto);
      else if (Fan_speed == "4")
        ac.setFan(kHitachiAc1FanLow);
      ac.setTemp(temperature.toInt());
      }
      //ac.on();
      
      ac.send();
    }

    if ((Power == "off"))
    {
    
     //ac.off();
      ac.begin();
      ac.setModel(R_LT0541_HTA_A);
      ac.setPower(false);
      //ac.setPower(true);
      ac.send();
    }
  }
}

bool reconnect(int timeout) {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");

    String mac = WiFi.softAPmacAddress();
    String clientId = mac.c_str();
    client.connect(mac.c_str(), mqtt_user, mqtt_pass);

    if (client.connect(mac.c_str())) {
      Serial.println("connected");

      client.subscribe(mac.c_str());
    }
    else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(3000);
    }
  }
  return true;
}

/**********************************************************************************************************************************************/

float read_temp()
{
  float temp;
  if (sht.readSample()) {
    temp = sht.getTemperature();
  }
  Serial.print("Temperature: ");
  Serial.println(temp);
  delay(100);
  return temp;
}

float read_humidity()
{
  if (sht.readSample()) {
    humidity = sht.getHumidity();
    Serial.print("Humidity: ");
    delay(100);
    return humidity;
  }
}


void send_DP(){
  int t;
  t = read_temp();
  String MAC_ID;
  timeClient.update();
  String time1 = String(timeClient.getFormattedTime());
  MAC_ID = WiFi.softAPmacAddress();
  Serial.print("MAC ID: ");
  Serial.println(MAC_ID);


  StaticJsonDocument<450> doc;

  //JsonObject doc = JSONbuffer.createNestedObject(MAC_ID.c_str());

  doc["m"] = MAC_ID;
  doc["v"] = String("AC_1." + String(version_no));
  doc["NS"] = WiFi.RSSI();
  doc["Tempc"] =String(read_temp());
  doc["Humidity"] = String(read_humidity());
  //doc["rate"] = String(change_rate);
  doc["Time"] = time1;

  char JSONmessageBuffer[450];
  serializeJson(doc, JSONmessageBuffer);
  Serial.println("Sending message to MQTT topic..");
  Serial.println(JSONmessageBuffer);
  if (client.publish("DP", JSONmessageBuffer) == true)
  { Serial.println("Success sending message");
  }
  else
  {
    Serial.println("Error sending message");
  }
}

/***********************************************************************************************************************************************/

void fn_ir_data(int i)
{
  int ind1_s = st_temp[i].indexOf(':');
  int ind1_l = st_temp[i].lastIndexOf('\n');
  st_data[i] = st_temp[i].substring(ind1_s + 2, ind1_l);
  Serial.println(st_data[i]);
}

String GetStringPartAtSpecificIndex(String StringToSplit, char SplitChar, int StringPartIndex)
{
  String originallyString = StringToSplit;
  String outString = "";
  for (int i1 = 0; i1 <= StringPartIndex; i1++)
  {
    outString = "";                   //if the for loop starts again reset the outString (in this case other part of the String is needed to take out)
    int SplitIndex = StringToSplit.indexOf(SplitChar);  //set the SplitIndex with the position of the SplitChar in StringToSplit

    if (SplitIndex == -1)               //is true, if no Char is found at the given Index
    {
      //outString += "Error in GetStringPartAtSpecificIndex: No SplitChar found at String '" + originallyString + "' since StringPart '" + (i1-1) + "'";    //just to find Errors
      return outString;
    }
    for (int i2 = 0; i2 < SplitIndex; i2++)
    {
      outString += StringToSplit.charAt(i2);      //write the char at Position 0 of StringToSplit to outString
    }
    StringToSplit = StringToSplit.substring(StringToSplit.indexOf(SplitChar) + 1);  //change the String to the Substring starting at the position+1 where last SplitChar found
  }
  return outString;
}


void setup() {

  esp32FOTA.checkURL = "https://living-things-split-ac.s3.ap-south-1.amazonaws.com/firmware.json";
  irsend.begin();
  sensor.begin();
#if defined(ESP8266)
  Serial.begin(kBaudRate, SERIAL_8N1, SERIAL_TX_ONLY);
#else
  Serial.begin(kBaudRate, SERIAL_8N1);
#endif
  while (!Serial)
    delay(50);
  Serial.printf("\n" D_STR_IRRECVDUMP_STARTUP "\n", kRecvPin);
#if DECODE_HASH

  irrecv.setUnknownThreshold(kMinUnknownSize);
#endif
  irrecv.enableIRIn();

  timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);

    /*WiFi.begin("BeIngenious", "Icapo@pvtltd");
       while (WiFi.status() != WL_CONNECTED) {
           delay(500);
           Serial.print(".");
       }
       Serial.println("");
       Serial.println("WiFi connected");
       Serial.println("IP address: ");
       Serial.println(WiFi.localIP());*/

  /*if (!SPIFFS.begin()) {
    Serial.println("Failed to mount file system");
    return;
  }
  load_config();
  while (rst_count <= 2)
  {
    WiFi.disconnect();
    setup_wifi(4000);
    rst_count = rst_count + 1;
  }*/

  if (!SPIFFS.begin()) {
      Serial.println("Failed to mount file system");
      return;
    }
    load_config();
    while (rst_count <= 2)
    {
      WiFi.disconnect();
      setup_wifi(4000);
      rst_count = rst_count + 1;
    }
    
  runner.init();
  Serial.println("Initialized scheduler");

  if (sht.init()) {
      Serial.print("init(): success\n");
    } else {
      Serial.print("init(): failed\n");
    }
    sht.setAccuracy(SHTSensor::SHT_ACCURACY_HIGH);
    runner.init();
    Serial.println("Initialized scheduler");
    
  runner.addTask(t1);
  Serial.println("added t1");
  t1.enable();
  Serial.println("Enabled t1");
  timeClient.update();
  String time1 = String(timeClient.getFormattedTime());

  String mac = WiFi.softAPmacAddress();
  Serial.print("SoftAP ACc ID:");
  Serial.print(mac);
  if (m == 0) {
    client.connect(mac.c_str(), mqtt_user, mqtt_pass);
    //client.setCallback(callback);
    client.subscribe(mac.c_str());
    reconnect(10000);
    MAC_ID = WiFi.softAPmacAddress();
    Serial.print("MAC ID: ");
    Serial.println(MAC_ID);


    StaticJsonDocument<450> doc;

    //JsonObject doc = JSONbuffer.createNestedObject(MAC_ID.c_str());

    doc["m"] = MAC_ID;
    doc["v"] = String("AC_1." + String(version_no));
    doc["NS"] = WiFi.RSSI();
    doc["Tempc"] =String(read_temp());
    doc["Time"] = time1;

    char JSONmessageBuffer[450];
    serializeJson(doc, JSONmessageBuffer);
    Serial.println("Sending message to MQTT topic..");
    Serial.println(JSONmessageBuffer);
    if (client.publish("DO", JSONmessageBuffer) == true)
    { Serial.println("Success sending message");
    }
    else
    {
      Serial.println("Error sending message");
    }
  }
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  pinMode(motionSensor, INPUT_PULLUP);
  // Set motionSensor pin as interrupt, assign interrupt function and set RISING mode
  attachInterrupt(digitalPinToInterrupt(motionSensor), detectsMovement, RISING);

}

void loop() {
  String time1 = String(timeClient.getFormattedTime());
  /********************************************************************************************************************************************/
  if (m == 1)
  {
    WiFiClient client = server.available();   // listen for incoming clients
    Serial.println("New Client.");
    Serial.println(client);
    int conf = 0;

    Serial.println(client.connected());
    if (client.connected() == 0)
    {
      if (millis() - timeout > 60000)
      {
        Serial.println("Client Timeout !");
        //client.stop();
        //return;
        //write_spiffs(WIFI_SSID, WIFI_PASSWORD, DEVICE_NAME, 1, String(digitalRead(MOC1)), String(digitalRead(MOC2)), String(digitalRead(MOC3)), String(digitalRead(MOC4)));
        ESP.restart();
      }
    }
    if (client) {                             // if you get a client,
      Serial.println("New Client.");           // print a message out the serial port
      String currentLine = "";                // make a String to hold incoming data from the client
      while (client.connected()) {            // loop while the client's connected


        if (client.available()) {             // if there's bytes to read from the client,
          timeout = 0;
          char c = client.read();             // read a byte, then
          Serial.write(c);                    // print it out the serial monitor
          if (c == '\n') {                    // if the byte is a newline character

            // if the current line is blank, you got two newline characters in a row.
            // that's the end of the client HTTP request, so send a response:
            if (currentLine.length() == 0) {
              // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
              // and a content-type so the client knows what's coming, then a blank line:
              client.println("HTTP/1.1 200 OK");
              client.println("Access-Control-Allow-Origin: *");
              client.println("Origin,X-Requested-With,Content-type,Accept");
              client.println();

              // the content of the HTTP response follows the header:

              // break out of the while loop:
              break;
            } else {    // if you got a newline, then clear currentLine:
              currentLine = "";
            }
          } else if (c != '\r') {  // if you got anything else but a carriage return character,
            currentLine += c;      // add it to the end of the currentLine
          }
          if (currentLine.endsWith("GET /0")) {     // Send device to online mode
            key = 2;
          }
          if (currentLine.startsWith("GET /23")) {  //Send request for reconfiguration
            conf = 1;
          }
          if (currentLine.startsWith("GET /details")) { // send device details to connected client
            key = 0;
          }
        }
      }
      if (key == 1)
      {
        StaticJsonDocument<450> JSONbuffer;
        JsonObject JSONencoder = JSONbuffer.to<JsonObject>();
        JSONencoder["m"] = WiFi.softAPmacAddress();
        JSONencoder["t"] = time1;
        /*JsonObject d = JSONbuffer.createNestedObject("d");

          d["0"] = digitalRead(MOC1);
          d["1"] = digitalRead(MOC2);
          d["2"] = digitalRead(MOC3);
          d["3"] = digitalRead(MOC4);
        */
        char JSONmessageBuffer[450];
        serializeJson(JSONbuffer, JSONmessageBuffer);
        Serial.println("Sending message to Intranet server");
        Serial.println(JSONmessageBuffer);
        //client.println(JSONmessageBuffer);
      }
      if (key == 2)
      {
        StaticJsonDocument<450> JSONbuffer;
        JsonObject JSONencoder = JSONbuffer.to<JsonObject>();
        JSONencoder["m"] = WiFi.softAPmacAddress();
        JSONencoder["t"] = time1;
        JsonObject d = JSONbuffer.createNestedObject("d");
        /*
          d["0"] = digitalRead(MOC1);
          d["1"] = digitalRead(MOC2);
          d["2"] = digitalRead(MOC3);
          d["3"] = digitalRead(MOC4);
        */
        char JSONmessageBuffer[450];
        serializeJson(JSONbuffer, JSONmessageBuffer);
        Serial.println("Sending message to Intranet server");
        Serial.println(JSONmessageBuffer);
        //client.println(JSONmessageBuffer);
      }
      if (key == 0)
      {
        StaticJsonDocument<225> JSONbuffer;
        JsonObject JSONencoder = JSONbuffer.to<JsonObject>();
        JSONencoder["m"] = WiFi.softAPmacAddress();
        char JSONmessageBuffer[225];
        serializeJson(JSONbuffer, JSONmessageBuffer);
        Serial.println("Sending message to Intranet server");
        Serial.println(JSONmessageBuffer);
        client.println(JSONmessageBuffer);
      }

      // close the connection:
      client.stop();
      Serial.println("Client Disconnected.");
    }
    if (conf == 1)
    {
      wifi_config();
      conf = 0;
    }
    if (key == 2)
    {
      ESP.restart();
    }

  }

  now = millis();
  // Turn off the LED after the number of seconds defined in the timeSeconds variable
  if (startTimer && (now - lastTrigger > (timeSeconds * 1000))) {
    Serial.println("Motion stopped...");
    flag_reset = 0;
    startTimer = false;
    Serial.print("flag_pir:");
    Serial.print(flag_pir);
    Serial.print("\n");
    Serial.print("flag_reset:");
    Serial.print(flag_reset);
    Serial.print("\n");
  }

  if (flag_pir == 0) {
    if (count_pir == 1200)
    {
      if (flag_reset == 0)
      {
        ac.begin();
        ac.off();
        ac.setTemp(temperature.toInt());
        String s = "kHitachiAc1";
        if (AC_Mode == "0")
          ac.setMode(kHitachiAc1Cool);
        else if (AC_Mode == "1")
          ac.setMode(kHitachiAc1Dry);
        else if (AC_Mode == "2") {
          ac.setMode(kHitachiAc1Auto);
          /*  ac.on();
             ac.setTemp(temperature.toInt());*/
          //irsend.sendRaw(rawData_auto, 583, 38);                                                                          // Send a raw data capture at 38kHz.
          //delay(2000);
        }
        else if (AC_Mode == "3") {
           ac.setMode(kHitachiAc1Heat);

          //irsend.sendRaw(rawData_hm, 583, 38);                                                                          // Send a raw data capture at 38kHz.
          //delay(2000);
        }
        else if (AC_Mode == "4")
           ac.setMode(kHitachiAc1Fan);
        
        String s1 = "kHitachiAc1Fan";
        if (Fan_speed == "1")
          ac.setFan(kHitachiAc1FanHigh);
        else if (Fan_speed == "2")
          ac.setFan(kHitachiAc1FanMed);
        else if (Fan_speed == "3")
          ac.setFan(kHitachiAc1FanAuto);
        else if (Fan_speed == "4")
          ac.setFan(kHitachiAc1FanLow);
        ac.send();
        //irsend.sendRaw(rawData_off, 583, 38);
        flag_pir = 1;
        timeout = 0;

      }
      count_pir = 0;
    }
    if (count_stop == 0) {
      count_pir = count_pir + 1;
      delay(100);
      //    Serial.print("count_pir:");
      //    Serial.print(count_pir);
      //    Serial.print("\n");
    }
  }
  if (flag_inturrupt == 1)
  {
    //send_pir_command();
    flag_inturrupt = 0;
    flag_pir = 0;
  }

  timeClient.update();
  runner.execute();

  client.loop();
  if (m == 0)
  {
    if (!client.connected())
    {
      reconnect(10000);
    }
  }
  if (irrecv.decode(&results)) {

    uint32_t now = millis();
    Serial.printf(D_STR_TIMESTAMP " : %06u.%03u\n", now / 1000, now % 1000);

    if (results.overflow)
      Serial.printf(D_WARN_BUFFERFULL "\n", kCaptureBufferSize);

    Serial.println(D_STR_LIBRARY "   : v" _IRREMOTEESP8266_VERSION_ "\n");

    Serial.print("Results_AC_Brand:");
    Serial.print(resultToHumanReadableBasic(&results));



    String description = IRAcUtils::resultAcToString(&results);
    String StringSplits[10];
    int ind1 = description.indexOf("Swing(V)");
    String temp = description.substring(0, ind1);

    if (description.length())

      Serial.println(D_STR_MESGDESC ": " + description);

    Serial.print("temp:");
    Serial.println(temp);

    for (int i = 0; i < 6; i++)  //this for loop runs 10 times, bus just the number of parts which are possible will be saved in StringSplits[i]
    {
      StringSplits[i] = GetStringPartAtSpecificIndex(temp, ',', i); //give this function your string to split, your char to split and the index of the StringPart you want to get
      Serial.print("Part ");
      Serial.print(i);
      Serial.print(":");
      Serial.println(StringSplits[i]);  //see it on serial monitor
      Serial.println("Done");
      st_temp[i] = StringSplits[i];
      fn_ir_data(i);
    }
    Serial.print("st_data[0]:");
    Serial.println(st_data[0]);

    Serial.print("st_data[1]:");
    Serial.println(st_data[1]);

    Serial.print("st_data[2]:");
    Serial.println(st_data[2]);

    Serial.print("st_data[3]:");
    Serial.println(st_data[3]);

    Serial.print("st_data[4]:");
    Serial.println(st_data[4]);

    Serial.print("st_data[5]:");
    Serial.println(st_data[5]);

    Serial.print("st_data[6]:");
    Serial.println(st_data[6]);


    /***************************************************************************************************************************/

    String MAC_ID;
    MAC_ID = WiFi.softAPmacAddress();
    Serial.print("MAC ID: ");
    Serial.println(MAC_ID);


    StaticJsonDocument<450> doc;

    doc["m"] = MAC_ID;
    doc["P"] = (st_data[1]);
    if(doc["P"] == "Off")
    {
      doc["P"] = "off";  
      Power = "off";
    }
    if(doc["P"] == "On"){
    
    String str3;
    String str5;
    int ct3;
    str3 = (st_data[3][0] - 48);
    str5= (st_data[3][1] - 48);
    ct3 = str3.length();
    if (str3 == "9")
    {
      doc["Mo"] = "3";
      AC_Mode = "3";
    }
    if (str3 == "1" && str5 == "4")
    {
      doc["Mo"] = "2";
      AC_Mode = "2";
    }

    if (str3 == "6")
    {
      doc["Mo"] = "0";
      AC_Mode = "0";
    }
    if (str3 == "2")
    {
      doc["Mo"] = "1";
      AC_Mode = "1";
    }
    if (str3 == "4")
    {
      doc["Mo"] = "4";
      AC_Mode = "4";
    }
    


    //Serial.print("st_data[1] length ");
    //Serial.println(st_data[1].length());



    String T1;
    int ct;
    T1 += st_data[4][0];
    T1 += st_data[4][1];
    if (T1 == "Of")
    {
      T1 = temperature;
    }
    else {
      doc["T"] = (T1);
    }
    ct = T1.length();
    if (ct == 0) {}
    else {
      temperature = T1;
    }
    String str2;
    int ct2;
    str2 = (st_data[5][0] - 48);
    ct = str2.length();
    if (str2 == "1")
    {
      doc["F"] = "3";
      Fan_speed = "3";
    }
    if (str2 == "8")
    {
      doc["F"] = "4";
      Fan_speed = "4";
    }

    if (str2 == "2")
    {
      doc["F"] = "1";
      Fan_speed = "1";
    }

   
    if (str2 == "4")
    {
      doc["F"] = "2";
      Fan_speed = "2";
    }
    }
    doc["Time"] = time1;

    int ct1;
    String str1;
    str1 = st_data[1];
    ct1 = str1.length();
    Serial.print(ct1);
    if (st_data[1] == "Off")
    {
      doc["P"] = "off";
      Power = "off";
      doc["Mo"] = "";
      doc["T"] = "";
      doc["F"] = "";
    }
    else
    {
      Power = "on";
    }
    //Serial.print("st_data[1] length ");
    //Serial.println(st_data[1].length());
    char JSONmessageBuffer[450];
    serializeJson(doc, JSONmessageBuffer);
    Serial.println("Sending message to MQTT topic..");
    Serial.println(JSONmessageBuffer);
    if (buf != 1)
    {

      if (client.publish("DATA_AC", JSONmessageBuffer) == true)
      { Serial.println("Success sending message");
      }
      else
      {
        Serial.println("Error sending message");
      }

    }
    /***********************************************************************************************************************************************************************/

#if LEGACY_TIMING_INFO

    Serial.println(resultToTimingInfo(&results));  /*******************************************************    no */
    yield();
#endif

    Serial.print("Results : Raw Data :   ");
    Serial.println(resultToSourceCode(&results));
    Serial.println();
    yield();
    buf = 0;
  }




  /**************************************************************************************************************/
  timerWrite(timer, 0); //reset timer (feed watchdog)
  long loopTime = millis();
  loopTime = millis() - loopTime;

  /********************************************************************************************/
  if (m == 0) {
    if (ota_update == 1) {
      bool updatedNeeded = esp32FOTA.execHTTPcheck();                                             /*****************************************************************************/
      if (updatedNeeded)
      {
        count_stop = 1;
        delay(4000);
        esp32FOTA.execOTA();
      }
    }
    ota_update = 0;
    //Serial.println(mqtt_server);
    //Serial.println("OTA Demo");
  }
}
